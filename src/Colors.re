let pink = Css.hex("D087B2");
let pinkLight = Css.hex("FAE8F2");

let purple = Css.hex("8D59B8");
let purpleLight = Css.hex("E6DDF4");
let purpleLighter = Css.hex("FCFAFC");

let yellow = Css.hex("FFC99F");

let orange = Css.hex("FF9C9C");

let green = Css.hex("6BCA4A");

let grayText = Css.hex("A4A4A4");